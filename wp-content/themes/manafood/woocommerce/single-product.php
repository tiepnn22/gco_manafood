<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @package     WooCommerce\Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>

<?php
    $product_id = get_the_ID();

	$terms = wp_get_object_terms($post->ID, 'product_cat');
	if (!is_wp_error($terms) && !empty($terms) && is_object($terms[0])) $term = $terms[0];
    $term_id        = $term->term_id;
    $term_name      = $term->name;
    $term_excerpt   = wpautop($term->description);
    $term_link      = esc_url(get_term_link($term_id));
    $taxonomy_slug  = $term->taxonomy;
    $thumbnail_id        = get_term_meta( $term_id, 'thumbnail_id', true ); // woo
    $term_image_check    = wp_get_attachment_url( $thumbnail_id ); // woo
    $term_image          = (!empty($term_image_check)) ? $term_image_check : ''; // woo

    //woocommerce
    $product = new WC_product($product_id);

    //gallery
    $single_product_gallery = $product->get_gallery_image_ids();
    
    //info product
    $single_product_title       = get_the_title($product_id);
    $single_product_date        = get_the_date('d/m/Y', $product_id);
    $single_product_link        = get_permalink($product_id);
    $single_product_image       = getPostImage($product_id,"full");
    $single_product_excerpt     = get_the_excerpt($product_id);
    $single_recent_author       = get_user_by( 'ID', get_post_field( 'post_author', get_the_author() ) );
    $single_product_author      = $single_recent_author->display_name;
    $single_product_tag         = get_the_tags($product_id);

    $post_comment       = wp_count_comments($product_id);
    $post_comment_total = $post_comment->total_comments;
?>

<?php get_template_part("resources/views/page-banner"); ?>

<section class="contact-page">
    <div class="mt-lg-5 mt-2">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-5">
                    <aside class="index-aside">
                        <?php dynamic_sidebar( 'sidebar-product' ); ?>
                    </aside>
                </div>
                <div class="col-lg-9 col-md-8 col-sm-7">
                    <div class="pdetail-intro">
                        <div class="row">
                            <div class="col-md-6 col-sm-6">
                                <div class="pdetail-slider">
                                    <div class="img_zoom">
                                        <a class="MagicZoom" id="allstar" href="<?php echo $single_product_image; ?>" data-options="selectorTrigger: hover;" onclick="return false;">
                                            <img src="<?php echo $single_product_image; ?>" alt="" />
                                        </a>
                                    </div>
                                    <div class="MagicScroll my-3" id="ZoomScroll" data-options="items: 3;autoplay: true;height:100px" data-mobile-options="orientation:horizontal;items:2">
                                        <a data-zoom-id="allstar" href="<?php echo $single_product_image; ?>" data-image="<?php echo $single_product_image; ?>">
                                            <img src="<?php echo $single_product_image; ?>" alt="">
                                        </a>

                                        <?php if(!empty( $single_product_gallery )) { ?>
                                        <?php
                                            foreach( $single_product_gallery as $single_product_gallery_kq ){

                                            $post_image = wp_get_attachment_url( $single_product_gallery_kq );
                                        ?>
                                            <a data-zoom-id="allstar" href="<?php echo $post_image; ?>" data-image="<?php echo $post_image; ?>" onclick="return false;">
                                                <img src="<?php echo $post_image; ?>" alt="" />
                                            </a>
                                        <?php } ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6">
                                <?php echo show_sale($product_id); ?>

                                <h2 class="s24 pdetail-tit"><?php echo $single_product_title; ?></h2>

                                <div class="s14 f2 d-flex align-items-center flex-wrap pdetail-rheader">
                                    <?php echo show_rating($product_id); ?>
                                    <span class="px-2 t6">|</span>
                                    
                                    <?php if(!empty( $post_comment_total )) { ?>
                                        <span><?php echo $post_comment_total; ?> đánh giá</span>
                                    <?php } ?>

                                    <span class="px-2 t6">|</span>
                                    <a href="#review" class="t5"><i class="fas fa-edit"></i> Viết đánh giá</a>
                                </div>

                                <div class="pdetail-content s14 t2 f2 wp-editor-fix">
                                    <p><?php echo $single_product_excerpt; ?></p>
                                </div>

                                <?php echo show_price_old_price($product_id); ?>

                                <div class="vk-shop-detail__qty">
                                    <?php echo show_add_to_cart_button_quantity($product_id); ?>
                                    <div class="mt-4 pdetail-quan">
                                        <?php echo show_add_to_cart_button_ajax($product_id); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pdetail-info">
                        <h2 class="s14 text-uppercase pdetail-info-tit"><span>Thông tin sản phẩm</span></h2>
                        <div class="s14 t2 f2 pt-3 pdetail-content wp-editor-fix">
                            <?php the_content(); ?>
                        </div>
                    </div>

                    <?php get_template_part("resources/views/template-related-product"); ?>

                    <div id="review" class="pdetail-review">
                        <div class="pdetail-review-wrap">
                            <?php
                                if ( comments_open() || get_comments_number() ) {
                                    comments_template();
                                }
                            ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>

<?php
get_footer( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */

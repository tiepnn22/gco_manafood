<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' ); ?>

<?php
    $term_info         = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
    $term_id           = $term_info->term_id;
    $term_name_check   = $term_info->name;
    $term_name		   = (!empty($term_name_check)) ? $term_name_check : 'Sản phẩm';
    $term_excerpt   = wpautop($term_info->description);
    $term_link      = esc_url(get_term_link($term_id));
    $taxonomy_slug  = $term_info->taxonomy;
    $thumbnail_id   = get_term_meta( $term_id, 'thumbnail_id', true );
    $term_image     = wp_get_attachment_url( $thumbnail_id );
?>

<?php get_template_part("resources/views/page-banner"); ?>

<section class="pro-wrap">
    <div class="mt-lg-5 mt-2">
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-4 col-sm-5">
                    <aside class="index-aside">
                        <?php dynamic_sidebar( 'sidebar-product' ); ?>
                    </aside>
                </div>

                <div class="col-lg-9 col-md-8 col-sm-7">
                    <?php if(!empty( $term_image )) { ?>
                    <div class="text-center pbanner">
                        <img src="<?php echo $term_image; ?>" alt="<?php echo $term_name; ?>">
                    </div>
                    <?php } ?>

                    <section class="hpro">
                        <h2 class="s30 pro-tit"><?php echo $term_name; ?></h2>

                        <?php if(!empty( $term_excerpt )) { ?>
                        <div class="hpro-sum"><?php echo $term_excerpt; ?></div>
                        <?php } ?>

                            <?php
                                if ( woocommerce_product_loop() ) {
                            ?>

                                <!--order-->
                                <div class="text-right hpro-control">
                                    Sắp xếp: <?php do_action( 'woocommerce_before_shop_loop' ); ?>
                                </div>

                                <div class="row pro-row">
                                    <?php
                                        // woocommerce_product_loop_start();
                                        if ( wc_get_loop_prop( 'total' ) ) {
                                            while ( have_posts() ) {
                                                the_post();

                                                // do_action( 'woocommerce_shop_loop' );
                                                // wc_get_template_part( 'content', 'product' );
                                                get_template_part('resources/views/content/category-product', get_post_format());
                                            }
                                        }
                                        // woocommerce_product_loop_end();
                                    ?>
                                </div>

                                <!--pagination-->
                                <?php do_action( 'woocommerce_after_shop_loop' ); ?>

                            <?php
                                    // do_action( 'woocommerce_after_shop_loop' );
                                } else {
                                    do_action( 'woocommerce_no_products_found' );
                                }
                            ?>

                    </section>
                </div>
            </div>
        </div>
    </div>
</section>

<?php

get_footer( 'shop' );

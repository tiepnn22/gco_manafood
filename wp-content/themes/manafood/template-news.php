<?php
	/*
	Template Name: Mẫu Tin tức
	*/
?>

<?php get_header(); ?>

<?php
    $page_id        = get_the_ID();
    $page_name      = get_the_title();
    $page_content   = wpautop(get_the_content());
?>

<?php get_template_part("resources/views/page-banner"); ?>

<section class="blog-wrap">
    <div class="mt-lg-5">
        <div class="container">
            <h1 class="sr-only"><?php echo $page_name; ?></h1>

            <?php
                $query = query_post_by_custompost_paged('post', 6);
                $max_num_pages = $query->max_num_pages;

                if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
            ?>

                <?php get_template_part('resources/views/content/category-post', get_post_format()); ?>

            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

            <?php echo paginationCustom( $max_num_pages ); ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>
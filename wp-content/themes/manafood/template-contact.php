<?php
	/*
	Template Name: Mẫu Liên hệ
	*/
?>

<?php get_header(); ?>

<?php
    $page_id        = get_the_ID();
    $page_name      = get_the_title();
    $page_content   = wpautop(get_the_content());

    //field
    $contact_contact_form_id    = get_field('contact_contact_form');
    $contact_contact_form       = do_shortcode('[contact-form-7 id="'.$contact_contact_form_id.'"]');

    $contact_map = get_field('contact_map');

    $customer_address   = get_field('customer_address', 'option');
    $customer_phone     = get_field('customer_phone', 'option');
    $customer_email     = get_field('customer_email', 'option');
    $customer_website   = get_field('customer_website', 'option');
?>

<?php get_template_part("resources/views/page-banner"); ?>

<section class="page-wrap">
    <div class="">
        <div class="container">
            <div class="s14 contact">
                <div class="maps">
                    <?php echo $contact_map; ?>
                </div>
                <div class="contact-wrap">
                    <h1 class="s30 contact-tit"><?php echo $page_name; ?></h1>
                    <div class="row justify-content-between">
                        <div class="col-lg-4 col-sm-6">
                            <ul class="list-unstyled ft-add">
                                <li><i class="fas fa-map-marker-alt"></i>
                                    <?php echo $customer_address; ?>
                                </li>
                                <li><i class="fas fa-phone"></i>
                                    <a href="tel:<?php echo str_replace(' ','',$customer_phone);?>" title=""><?php echo $customer_phone; ?></a>
                                </li>
                                <li><i class="fas fa-envelope"></i>
                                    <a href="mailto:<?php echo $customer_email; ?>" title=""><?php echo $customer_email; ?></a>
                                </li>
                                <li><i class="fas fa-globe"></i>
                                    <a href="<?php echo $customer_website; ?>" title=""><?php echo $customer_website; ?></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-sm-6">
                            <?php if(!empty( $contact_contact_form )) { ?>
                            <div class='contact-frm'>
                                <?php echo $contact_contact_form; ?>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
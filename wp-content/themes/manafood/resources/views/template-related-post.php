<?php
    global $post;
    $categories = get_the_category($post->ID);

    $category_ids = array();
    foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;
    $args=array(
	    'category__in'         => $category_ids,
	    'post__not_in'         => array($post->ID),
	    'posts_per_page'       => 6,
	    'ignore_sticky_posts'  => 1
    );
    $query = new wp_query( $args );
?>


<div class="col-lg-3 col-md-4">
    <aside class="blogaside">
        <h2 class="s14 text-uppercase pdetail-info-tit"><span>bài viết liên quan</span></h2>
        <ul class="list-unstyled bdetail-list">

            <?php
                if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
            ?>

                <?php get_template_part('resources/views/content/related-post', get_post_format()); ?>

            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>

        </ul>
    </aside>
</div>
<?php
    //cart
    $info_cart     = WC()->cart->cart_contents;
    $cart_count    = WC()->cart->get_cart_contents_count();
    $cart_total    = WC()->cart->get_cart_total();
    $cart_page_url = wc_get_cart_url();

    //checkout
    $checkout_page_url = wc_get_checkout_url();
?>

<div class="ml-5">
    <div class="top-cart">
        <a class="position-relative top-cart-btn" href="<?php echo $cart_page_url; ?>" title="" data-toggle="dropdown">
            <img src="<?php echo asset('images/cart.png'); ?>" alt="">
            <span class="t2 s14 pl-2 top-cart-quan">
                Giỏ hàng(<?php if($cart_count > 0) { echo $cart_count; } else { echo 0; } ?>)
            </span>
        </a>

        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
            <?php if($cart_count > 0) { ?>

                <?php
                    foreach ($info_cart as $info_cart_kq) {
                        
                    $product_id         = $info_cart_kq["product_id"];
                    $product_title      = get_the_title($product_id);
                    $product_link       = get_permalink($product_id);
                    $product_image      = getPostImage($product_id,"p-product");

                    $product_quantity   = $info_cart_kq["quantity"];
                    $product_total      = $info_cart_kq["line_total"];
                    $product_price      = format_price_donvi($product_total / $product_quantity);
                ?>

                    <div class="dropdown-item d-flex align-items-center cart-top-item single-cart-item">
                        <a class="" href="<?php echo $product_link; ?>">
                            <img src="<?php echo $product_image; ?>" alt="<?php echo $product_title; ?>">
                        </a>
                        <div class="cart-top-info">
                            <h2 class="cart-top-name text-truncate">
                                <a href="<?php echo $product_link; ?>" title="<?php echo $product_title; ?>">
                                    <?php echo $product_title; ?>
                                </a>
                                <br><?php echo $product_quantity; ?> x <strong><?php echo $product_price; ?></strong>
                            </h2>
                            <p class="text-right trash" data-productid="<?php echo $product_id; ?>">
                                <i class="fa fa-trash top-cart-del"></i>
                            </p>
                        </div>
                    </div>

                <?php } ?>

                <div class="dropdown-item cart-top-total">
                    TỔNG <strong class="pull-right"><?php echo $cart_total; ?></strong>
                </div>
                <div class="dropdown-item cart-top-item">
                    <a href="<?php echo $cart_page_url; ?>" title="" class="text-center btn">Xem giỏ hàng</a>
                </div>

            <?php } else { ?>
                <div class="p-3">
                    <?php _e('Giỏ hàng trống !', 'text_domain'); ?>
                </div>
            <?php } ?>
        </div>
    </div>
</div>
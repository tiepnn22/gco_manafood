<?php
	$post_id            = get_the_ID();
	$post_title 		= get_the_title($post_id);
	// $post_content 		= wpautop(get_the_content($post_id));
	$post_date 			= get_the_date('d/m/Y',$post_id);
	$post_link 			= get_permalink($post_id);
	$post_image 		= getPostImage($post_id,"p-product");
	$post_excerpt 		= cut_string(get_the_excerpt($post_id),200,'...');
	$post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
	$post_tag 			= get_the_tags($post_id);
?>

<div class="col-lg-3 col-md-4 col-sm-6">
    <article class="text-center position-relative hslider-item">
        <figure class="text-center hslider-item-img">
            <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
            	<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
            </a>
            <div class="text-center sale-item-act">
                <a href="javascript:void(0)" class="btn hover-product" data-productid="<?php echo $post_id; ?>">
                	<i class="fas fa-eye"></i>
                </a>
                <a title="" href="javascript:void(0)" class="btn text-uppercase buy-btn" tabindex="0">
                	<?php echo show_add_to_cart_button($post_id); ?>
                </a>
            </div>
        </figure>
        <figcaption class="pitem hslider-info">
            <h3 class=" s14 ptit">
            	<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
	            	<?php echo $post_title; ?>
	            </a>
	        </h3>
	        <?php echo show_price_old_price($post_id); ?>
            <?php echo show_sale($post_id); ?>
        </figcaption>
    </article>
</div>
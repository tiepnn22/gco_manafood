<?php
	$post_id            = get_the_ID();
	$post_title 		= get_the_title($post_id);
	// $post_content 		= wpautop(get_the_content($post_id));
	$post_date 			= get_the_date('d m Y',$post_id);
	$post_link 			= get_permalink($post_id);
	$post_image 		= getPostImage($post_id,"p-post");
	$post_excerpt 		= cut_string(get_the_excerpt($post_id),400,'...');
	$post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
	$post_tag 			= get_the_tags($post_id);
?>

<div class="blog-item">
    <div class="row">
        <div class="col-sm-4">
            <div class="text-center blog-img">
            	<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
            		<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>" class="lazy">
            	</a>
            </div>
        </div>
        <div class="col-lg-8">
            <figcaption class="blog-info">
                <h3 class="s18 bold blog-info-tit">
                	<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>" tabindex="0">
                		<?php echo $post_title; ?>
                	</a>
                </h3>
                <h4 class="blog-time t1 s14">
                	<img src="<?php echo asset('images/icon4.png'); ?>" alt=""> <?php echo $post_date; ?>
                </h4>
                <div class="blog-info-content">
                    <p><?php echo $post_excerpt; ?></p>
                </div>
            </figcaption>
        </div>
    </div>
</div>
<?php
class show_list_taxonomy_product extends WP_Widget {
    function __construct() {
        parent::__construct(
            'show_list_taxonomy_product',
            'Core - Hiển thị danh mục sản phẩm',
            array( 'description'  =>  'Hiển thị danh mục sản phẩm' )
        );
    }
    function form( $instance ) {
        $default = array(
            'title' => 'Hiển thị danh mục sản phẩm',
        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);

        echo '<p>';
            echo 'Tiêu đề :';
            echo '<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/>';
        echo '</p>';
    }
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        return $instance;
    }
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $widget_id = $args["widget_id"];

        //field
        $widget_taxonomy_product_select   = get_field('widget_taxonomy_product_select', 'widget_' . $widget_id);


        echo $before_widget; ?>
        <div class="index-aside-item">
            <h2 class="tit green s16  text-white text-uppercase"><span><?php echo $title; ?></span></h2>
            <ul class="b1 cate-list">

                <?php
                    foreach ($widget_taxonomy_product_select as $foreach_kq) {

                    $term_id        = $foreach_kq->term_id;
                    $term_desc      = cut_string( $foreach_kq->description ,300,'...');
                    $taxonomy_slug  = $foreach_kq->taxonomy;
                    $term_name      = get_term( $term_id, $taxonomy_slug )->name;
                    $term_link      = get_term_link(get_term( $term_id ));
                    // $thumbnail_id = get_term_meta( $term_id, 'thumbnail_id', true );
                    // $term_image   = wp_get_attachment_url( $thumbnail_id ); // woo

                    $query = query_post_by_taxonomy_paged('product', $taxonomy_slug, $term_id, -1);
                    $total_post = $query->found_posts;
                ?>
                    <li class="d-flex align-items-center justify-content-between flex-wrap">
                        <a href="<?php echo $term_link; ?>" title="<?php echo $term_name; ?>"><?php echo $term_name; ?></a> 
                        <span>(<?php echo $total_post; ?>)</span>
                    </li>
                <?php } ?>

            </ul>
        </div>
        <?php echo $after_widget;
    }
}
function create_showlisttaxonomyproduct_widget() {
    register_widget('show_list_taxonomy_product');
}
add_action( 'widgets_init', 'create_showlisttaxonomyproduct_widget' );
?>
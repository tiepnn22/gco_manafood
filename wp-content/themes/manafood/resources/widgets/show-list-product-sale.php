<?php
class show_list_product_sale extends WP_Widget {
    function __construct() {
        parent::__construct(
            'show_list_product_sale',
            'Core - Hiển thị các sản phẩm sale',
            array( 'description'  =>  'Hiển thị các sản phẩm sale' )
        );
    }
    function form( $instance ) {
        $default = array(
            'title' => 'Hiển thị các sản phẩm sale',
        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);

        echo '<p>';
            echo 'Tiêu đề :';
            echo '<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/>';
        echo '</p>';
    }
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        return $instance;
    }
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $widget_id = $args["widget_id"];

        //field
        $widget_product_select   = get_field('widget_product_select', 'widget_' . $widget_id);


        echo $before_widget; ?>
        <div id="saleday" class="">
            <h1 class="tit yellow s16 text-uppercase"><span><?php echo $title; ?></span></h1>
            <div class="sale-slider">

                <?php if(!empty( $widget_product_select )) { ?>
                <?php
                    foreach ($widget_product_select as $foreach_kq) {

                    $post_id            = $foreach_kq->ID;
                    $post_title         = get_the_title($post_id);
                    // $post_content        = wpautop(get_the_content($post_id));
                    $post_date          = get_the_date('d/m/Y',$post_id);
                    $post_link          = get_permalink($post_id);
                    $post_image         = getPostImage($post_id,"p-product");
                    $post_excerpt       = cut_string(get_the_excerpt($post_id),200,'...');
                    $post_author        = get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
                    $post_tag           = get_the_tags($post_id);
                ?>
                    <div class="saleday-item">
                        <div class="row">
                            <div class="col-4">
                                <a title="<?php echo $post_title; ?>" class="sale-item-img">
                                    <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
                                </a>
                                <div class="text-center sale-item-act">
                                    <a href="javascript:void(0)" class="btn hover-product" data-productid="<?php echo $post_id; ?>">
                                        <i class="fas fa-eye"></i>
                                    </a>
                                    <a title="" href="javascript:void(0)" class="btn text-uppercase buy-btn" tabindex="0">
                                        <?php echo show_add_to_cart_button($post_id); ?>
                                    </a>
                                </div>
                            </div>
                            <div class="col-8">
                                <div class="pitem">
                                    <h3 class=" s14 ptit">
                                        <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
                                            <?php echo $post_title; ?>
                                        </a>
                                    </h3>
                                    <?php echo show_price_old_price($post_id); ?>
                                    <?php echo show_sale($post_id); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php } ?>

            </div>
        </div>
        <?php echo $after_widget;
    }
}
function create_showlistproductsale_widget() {
    register_widget('show_list_product_sale');
}
add_action( 'widgets_init', 'create_showlistproductsale_widget' );
?>
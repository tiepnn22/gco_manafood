<?php
class show_list_testimonial extends WP_Widget {
    function __construct() {
        parent::__construct(
            'show_list_testimonial',
            'Core - Hiển thị lời bình',
            array( 'description'  =>  'Hiển thị lời bình' )
        );
    }
    function form( $instance ) {
        $default = array(
            'title' => 'Hiển thị lời bình',
        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);

        echo '<p>';
            echo 'Tiêu đề :';
            echo '<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/>';
        echo '</p>';
    }
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        return $instance;
    }
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $widget_id = $args["widget_id"];

        //field
        $widget_testimonial   = get_field('widget_testimonial', 'widget_' . $widget_id);


        echo $before_widget; ?>
        <div class="index-aside-item">
            <h2 class="tit red s16  text-white text-uppercase"><span><?php echo $title; ?></span></h2>
            <div class="tes-slider">

                <?php if(!empty( $widget_testimonial )) { ?>
                <?php
                    foreach ($widget_testimonial as $foreach_kq) {

                    $post_image = $foreach_kq["image"];
                    $post_title = $foreach_kq["title"];
                    $post_desc  = $foreach_kq["desc"];
                ?>
                    <div class="text-center  tes-item">
                        <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
                        <h3 class="s16 bold tes-item-name"><?php echo $post_title; ?></h3>
                        <div class="s14 tes-content">
                            <p><?php echo $post_desc; ?></p>
                        </div>
                    </div>
                <?php } ?>
                <?php } ?>
                
            </div>
        </div>
        <?php echo $after_widget;
    }
}
function create_showlisttestimonial_widget() {
    register_widget('show_list_testimonial');
}
add_action( 'widgets_init', 'create_showlisttestimonial_widget' );
?>
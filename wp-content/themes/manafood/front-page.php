<?php get_header(); ?>

<h1 style="display: none;"><?php echo get_option('blogname'); ?> - <?php echo get_option('blogdescription'); ?></h1>

<?php
    //field
    $home_slide = get_field('home_slide');

    $home_ads_content = get_field('home_ads_content');

    $home_product_feature_title        	= get_field('home_product_feature_title');
    $home_product_feature_select_post	= get_field('home_product_feature_select_post');
    $home_product_feature_image_ads  	= get_field('home_product_feature_image_ads');
    $home_product_feature_image_ads_url	= get_field('home_product_feature_image_ads_url');

    $home_product_buy_hot_title 		= get_field('home_product_buy_hot_title');
    $home_product_buy_hot_select_post 	= get_field('home_product_buy_hot_select_post');
    $home_product_buy_hot_image_ads 	= get_field('home_product_buy_hot_image_ads');

    $home_news_title        = get_field('home_news_title');
    $home_news_select_post  = get_field('home_news_select_post');
?>

<?php if(!empty( $home_slide )) { ?>
<section class="container-flush">
    <div id="slider" class="index-slider">

        <?php
            foreach ($home_slide as $foreach_kq) {

            $post_image = $foreach_kq["image"];
            $post_link  = $foreach_kq["url"];
        ?>
			<a href="<?php echo $post_link; ?>" title="">
				<img class="slider-img" src="<?php echo $post_image; ?>" alt="slider-img" />
			</a>
        <?php } ?>

    </div>
</section>
<?php } ?>

<?php if(!empty( $home_ads_content )) { ?>
<section class="ads">
    <div class="container">
        <div class="row">

	        <?php
	            foreach ($home_ads_content as $foreach_kq) {

	            $post_image = $foreach_kq["image"];
	            $post_link  = $foreach_kq["url"];
	        ?>
				<div class="col-lg-3 col-md-6 col-sm-6">
					<a href="<?php echo $post_link; ?>" title="" class="link-ef">
						<img src="<?php echo $post_image; ?>" alt="" class="lazy">
					</a>
				</div>
	        <?php } ?>

        </div>
    </div>
</section>
<?php } ?>

<section class="index-wrap">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-4 col-sm-5">
                <aside class="index-aside">
                    <?php dynamic_sidebar( 'sidebar-product' ); ?>
                </aside>
            </div>

            <div class="col-lg-9 col-md-8 col-sm-7">
                <section class="hpro-section">
                    <h2 class="tit yellow s16 text-uppercase"><span><?php echo $home_product_feature_title; ?></span></h2>
                    <div class="hot-slider">

                        <?php if(!empty( $home_product_feature_select_post )) { ?>
                        <?php
                            foreach ($home_product_feature_select_post as $foreach_kq) {

                            $post_id            = $foreach_kq->ID;
							$post_title 		= get_the_title($post_id);
							$post_date 			= get_the_date('d/m/Y',$post_id);
							$post_link 			= get_permalink($post_id);
							$post_image 		= getPostImage($post_id,"p-product");
							$post_excerpt 		= cut_string(get_the_excerpt($post_id),200,'...');
							$post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
							$post_tag 			= get_the_tags($post_id);
                        ?>

	                        <article class="text-center position-relative hslider-item">
	                            <figure class="text-center hslider-item-img">
						            <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
						            	<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
						            </a>
	                                <div class="text-center sale-item-act">
							            <a href="javascript:void(0)" class="btn hover-product" data-productid="<?php echo $post_id; ?>">
							                <i class="fas fa-eye"></i>
							            </a>
						                <a title="" href="javascript:void(0)" class="btn text-uppercase buy-btn" tabindex="0">
						                	<?php echo show_add_to_cart_button($post_id); ?>
						                </a>
	                                </div>
	                            </figure>
	                            <figcaption class="pitem hslider-info">
						            <h3 class=" s14 ptit">
						            	<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
							            	<?php echo $post_title; ?>
							            </a>
							        </h3>
							        <?php echo show_price_old_price($post_id); ?>
						            <?php echo show_sale($post_id); ?>
	                            </figcaption>
	                        </article>

                        <?php } ?>
                        <?php } ?>

                    </div>
                </section>

                <?php if(!empty( $home_product_feature_image_ads )) { ?>
                <div class="text-center mb-4 banner">
                    <a class="link-ef" href="<?php echo $home_product_feature_image_ads_url; ?>" title="">
                    	<img src="<?php echo $home_product_feature_image_ads; ?>" alt="">
                    </a>
                </div>
                <?php } ?>

                <section class="mt-3 bs-pro">
                    <h2 class="tit red s16 text-white text-uppercase"><span><?php echo $home_product_buy_hot_title; ?></span></h2>
                    <div class="bs-slider">

                        <?php if(!empty( $home_product_buy_hot_select_post )) { ?>
                        <?php
                            foreach ($home_product_buy_hot_select_post as $foreach_kq) {

                            $post_id            = $foreach_kq->ID;
							$post_title 		= get_the_title($post_id);
							$post_date 			= get_the_date('d/m/Y',$post_id);
							$post_link 			= get_permalink($post_id);
							$post_image 		= getPostImage($post_id,"p-product");
							$post_excerpt 		= cut_string(get_the_excerpt($post_id),200,'...');
							$post_author 		= get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
							$post_tag 			= get_the_tags($post_id);
                        ?>

	                        <div class="saleday-item">
	                            <div class="row no-gutters">
	                                <div class="col-4">
							            <a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
							            	<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
							            </a>
	                                    <div class="text-center sale-item-act">
								            <a href="javascript:void(0)" class="btn hover-product" data-productid="<?php echo $post_id; ?>">
								                <i class="fas fa-eye"></i>
								            </a>
											<a title="" href="javascript:void(0)" class="btn text-uppercase buy-btn" tabindex="0">
												<?php echo show_add_to_cart_button($post_id); ?>
											</a>
	                                    </div>
	                                </div>
	                                <div class="col-8">
	                                    <div class="pitem">
								            <h3 class=" s14 ptit">
								            	<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
									            	<?php echo $post_title; ?>
									            </a>
									        </h3>
									        <?php echo show_price_old_price($post_id); ?>
								            <?php echo show_sale($post_id); ?>
	                                    </div>
	                                </div>
	                            </div>
	                        </div>

                        <?php } ?>
                        <?php } ?>

                    </div>
                </section>

				<?php if(!empty( $home_product_buy_hot_image_ads )) { ?>
                <div class="banner man">
                    <div class="card-columns two-column">

				        <?php
				            foreach ($home_product_buy_hot_image_ads as $foreach_kq) {

				            $post_image = $foreach_kq["image"];
				            $post_link  = $foreach_kq["url"];
				        ?>
	                        <div class="card rounded-0">
	                        	<a class="" href="<?php echo $post_link; ?>" title="">
		                            <img src="<?php echo $post_image; ?>" class="" alt="">
		                        </a>
	                        </div>
				        <?php } ?>

                    </div>
                </div>
				<?php } ?>

                <section class="bs-pro">
                    <h2 class="tit green s16 text-white text-uppercase"><span><?php echo $home_news_title; ?></span></h2>
                    <div class="blog-slider">
                      
                        <?php if(!empty( $home_news_select_post )) { ?>
                        <?php
                            foreach ($home_news_select_post as $foreach_kq) {

                            $post_id            = $foreach_kq->ID;
                            $post_title         = get_the_title($post_id);
                            $post_date 			= get_the_date('d m Y',$post_id);
                            $post_link          = get_permalink($post_id);
                            $post_image         = getPostImage($post_id,"p-post");
                            $post_excerpt       = cut_string(get_the_excerpt($post_id),200,'...');
                            $post_author        = get_the_author_meta( 'nicename', get_the_author_meta( get_the_author() ) );
                            $post_tag           = get_the_tags($post_id);
                        ?>
	                        <article class="blog-item">
	                            <figure class="blog-img text-center">
					            	<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
					            		<img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>" class="lazy">
					            	</a>
	                            </figure>
	                            <figcaption class=" blog-info">
	                                <h3 class="s18 bold blog-info-tit">
					                	<a href="<?php echo $post_link; ?>" title="<?php echo $post_title; ?>">
					                		<?php echo $post_title; ?>
					                	</a>
	                                </h3>
	                                <h4 class="blog-time t1 s14">
	                                	<img src="<?php echo asset('images/icon4.png'); ?>" alt=""> <?php echo $post_date; ?>
	                                </h4>
	                                <div class="blog-info-content">
	                                    <p><?php echo $post_excerpt; ?></p>
	                                </div>
	                            </figcaption>
	                        </article>
                        <?php } ?>
                        <?php } ?>

                    </div>
                </section>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>


<?php get_header(); ?>

<?php
	$s = $_GET['s'];
?>

<?php get_template_part("resources/views/page-banner"); ?>

<section class="pro-wrap">
    <div class="mt-lg-5 mt-2">
        <div class="container">
            <div class="row">

                <div class="col-lg-3 col-md-4 col-sm-5">
                    <aside class="index-aside">
                        <?php dynamic_sidebar( 'sidebar-product' ); ?>
                    </aside>
                </div>

                <div class="col-lg-9 col-md-8 col-sm-7">
                    <section class="hpro">
                        <h2 class="s30 pro-tit">Sản phẩm</h2>

                        <div class="row pro-row">
                            <?php
                                $query = query_search_post_paged($s, array('product'), 16);
                                $max_num_pages = $query->max_num_pages;

                                if($query->have_posts()) : while ($query->have_posts() ) : $query->the_post();
                            ?>

                                <?php get_template_part('resources/views/content/category-product', get_post_format()); ?>

                            <?php endwhile; wp_reset_postdata(); else: echo ''; endif; ?>
                        </div>

                        <?php echo paginationCustom( $max_num_pages ); ?>
                    </section>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>
    </main>
</div>

<?php
    //field
    $customer_address   = get_field('customer_address', 'option');
    $customer_phone     = get_field('customer_phone', 'option');
    $customer_email     = get_field('customer_email', 'option');
    $customer_website   = get_field('customer_website', 'option');

    $f_service = get_field('f_service', 'option');

    $f_form_title   = get_field('f_form_title', 'option');
    $f_form_id      = get_field('f_form', 'option');
    $f_form         = do_shortcode('[contact-form-7 id="'.$f_form_id.'"]');

    $f_socical_facebook   = get_field('f_socical_facebook', 'option');
    $f_socical_twitter    = get_field('f_socical_twitter', 'option');
    $f_socical_google     = get_field('f_socical_google', 'option');
    $f_socical_insta      = get_field('f_socical_insta', 'option');

    $f_bottom_copyright   = get_field('f_bottom_copyright', 'option');
?>

<footer class="b3 t4 f2 s14 ft">

    <?php if(!empty( $f_service )) { ?>
    <div class="py-3 ">
        <div class="container">
            <div class="ft-intro">
                <div class="row">

                    <?php
                        foreach ($f_service as $foreach_kq) {

                        $post_image = $foreach_kq["image"];
                        $post_title = $foreach_kq["title"];
                        $post_desc  = $foreach_kq["desc"];
                    ?>
                        <div class="col-lg-4">
                            <div class="d-flex align-items-center intro-item">
                                <img src="<?php echo $post_image; ?>" alt="<?php echo $post_title; ?>">
                                <div class=" pl-4 intro-content">
                                    <h2 class="bold s14 text-uppercase intro-content-tit"><?php echo $post_title; ?></h2>
                                    <h3 class="s15"><?php echo $post_desc; ?></h3>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
    <?php } ?>

    <div class="container">
        <div class="ft-1">
            <div class="row justify-content-between">
                <div class="col-lg-3 col-md-6">
                    <ul class="list-unstyled ft-add">
                        <li><i class="fas fa-map-marker-alt"></i>
                            <?php echo $customer_address; ?>
                        </li>
                        <li><i class="fas fa-phone"></i>
                            <a href="tel:<?php echo str_replace(' ','',$customer_phone);?>" title=""><?php echo $customer_phone; ?></a>
                        </li>
                        <li><i class="fas fa-envelope"></i>
                            <a href="mailto:<?php echo $customer_email; ?>" title=""><?php echo $customer_email; ?></a>
                        </li>
                        <li><i class="fas fa-globe"></i>
                            <a href="<?php echo $customer_website; ?>" title=""><?php echo $customer_website; ?></a>
                        </li>
                    </ul>
                </div>

                <div class="col-lg-2 col-md-6">
                    <h2 class="text-uppercase s16 ft-tit"><?php echo wp_get_nav_menu_name("f_info" ); ?></h2>
                    <?php
                        if(function_exists('wp_nav_menu')){
                            $args = array(
                                'theme_location'    =>  'f_info',
                                'container'         =>  'nav',
                                'container_class'   =>  'nav-f_info',
                                'container_id'      =>  'nav-f_info',
                                'menu_class'        =>  'list-unstyled ft-list',
                            );
                            wp_nav_menu( $args );
                        }
                    ?>
                </div>
                <div class="col-lg-2 col-md-6">
                    <h2 class="text-uppercase  s16 ft-tit"><?php echo wp_get_nav_menu_name("f_support" ); ?></h2>
                    <?php
                        if(function_exists('wp_nav_menu')){
                            $args = array(
                                'theme_location'    =>  'f_support',
                                'container'         =>  'nav',
                                'container_class'   =>  'nav-f_support',
                                'container_id'      =>  'nav-f_support',
                                'menu_class'        =>  'list-unstyled ft-list',
                            );
                            wp_nav_menu( $args );
                        }
                    ?>
                </div>

                <div class="col-lg-3 col-md-6">
                    <h2 class="text-uppercase  s16 ft-tit"><?php echo $f_form_title; ?></h2>
                    <?php if(!empty( $f_form )) { ?>
                    <div class='ft-frm'>
                        <?php echo $f_form; ?>
                    </div>
                    <?php } ?>

                    <ul class="list-unstyled d-flex flex-wrap align-items-center justify-content-center justify-content-md-start color social">
                        <li>
                            <a href="<?php echo $f_socical_facebook; ?>" title="" target="_blank">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $f_socical_twitter; ?>" title="" target="_blank">
                                <i class="fab fa-twitter"></i>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $f_socical_google; ?>" title="" target="_blank">
                                <i class="fab fa-google-plus-g"></i>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo $f_socical_insta; ?>" title="" target="_blank">
                                <i class="fab fa-instagram"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="b4 ft-last">
        <div class="container">
            <h2 class="text-center"><?php echo $f_bottom_copyright; ?></h2>
        </div>
    </div>
</footer>

</div>

<!-- quickview -->
<div class="modal qv-modal" id="qv" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">

        </div>
    </div>
</div>
<div class="ajaxLoad"></div>

<?php get_template_part("resources/views/socical-footer"); ?>

<?php wp_footer(); ?>
</body>
</html>
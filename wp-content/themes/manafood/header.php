<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <link rel="canonical" href="<?php echo get_page_link_current(); ?>" />
    <title><?php echo title(); ?></title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>


<script type="text/javascript">
    var ajax_url = '<?php echo admin_url('admin-ajax.php'); ?>';
    var path_dist = '<?php echo get_template_directory_uri(); ?>/dist/';
</script>


<?php
    //field
    $customer_phone = get_field('customer_phone', 'option');

    $h_sale_on_day_title    = get_field('h_sale_on_day_title', 'option');
    $h_sale_on_day_url      = get_field('h_sale_on_day_url', 'option');
?>

<div class="wrapper">

<div class="s14 f1 t2 py-3 top-contact">
    <div class="container">
        <div class="d-flex align-items-center justify-content-between">
            <a href="tel:<?php echo str_replace(' ','',$customer_phone);?>" title="">
                <i class="fas fa-phone-square"></i> <?php echo $customer_phone; ?>
            </a>
            <!-- <span>
                <i class="fas fa-user"></i><a href="login.html" title="">Đăng nhập</a>
                <span>|</span>
                <a href="regis.html" title="">Đăng ký</a>
            </span> -->
        </div>
    </div>
</div>

<div class="swrap">

    <header class="top">
        <div class="swrap">
            <div class="container">
                <div class="w-100 d-flex align-items-center justify-content-between top-menu-control">

                    <div class="d-flex align-items-center justify-content-between top-menu-btn">
                        <!-- hamburger menu -->
                        <a id="nav-icon" href="#menu" class="d-xl-none">
                            <span></span>
                            <span></span>
                            <span></span>
                        </a>
                        <?php get_template_part("resources/views/logo"); ?>
                    </div>

                    <div class="d-flex align-items-center top-search">
                        <?php get_template_part("resources/views/search-form"); ?>

                        <?php get_template_part("resources/views/wc/wc-info-cart"); ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="swrap b1 top-menu">
            <div class="container">
                <div class="d-flex align-items-center justify-content-between">
                    <?php get_template_part("resources/views/menu"); ?>

                    <?php if(!empty( $h_sale_on_day_title )) { ?>
                        <a href="<?php echo $h_sale_on_day_url; ?>" class="btn sale-btn index-link" title="">
                            <?php echo $h_sale_on_day_title; ?>
                        </a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </header>

    <main class="index">
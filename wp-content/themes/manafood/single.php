<?php get_header(); ?>

<?php
	$post_id = get_the_ID();

	$get_category = get_the_category($post_id);
	foreach ( $get_category as $get_category_kq ) {
	    $cat_id   = $get_category_kq->term_id;
	}
	$cat_name = get_cat_name($cat_id);
	
	//info post
	$single_post_title 		= get_the_title($post_id);
	$single_post_date 		= get_the_date('d m Y', $post_id);
	$single_post_link 		= get_permalink($post_id);
    $single_post_image 		= getPostImage($post_id,"full");
	$single_post_excerpt 	= cut_string(get_the_excerpt($post_id),300,'...');
	$single_recent_author 	= get_user_by( 'ID', get_post_field( 'post_author', get_the_author() ) );
	$single_post_author 	= $single_recent_author->display_name;
    $single_post_tag 		= get_the_tags($post_id);
?>

<?php get_template_part("resources/views/page-banner"); ?>

<section class="blog-wrap">
	<div class="mt-lg-5 mt-2">
	    <div class="container">
	        <div class="row">
	            <div class="col-lg-9 col-md-8">
	                <h1 class="s30 pb-4 bdetail-tit"><?php echo $single_post_title; ?></h1>
	                <h4 class="blog-time t1 s14">
	                	<img src="<?php echo asset('images/icon4.png'); ?>" alt=""> <?php echo $single_post_date; ?>
	                </h4>
	                <div class="bdetail-content wp-editor-fix">
	                    <?php the_content(); ?>
	                </div>

	                <?php get_template_part("resources/views/socical-bar"); ?>
	            </div>

	            <?php get_template_part("resources/views/template-related-post"); ?>
	        </div>
	    </div>
	</div>
</section>

<?php get_footer(); ?>
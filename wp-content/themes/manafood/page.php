<?php get_header(); ?>

<?php
	$page_id       = get_the_ID();
	$page_name     = get_the_title();
	$page_content  = get_the_content(); //woo phải dùng the_content()
?>

<?php get_template_part("resources/views/page-banner"); ?>

<section class="page-wrap">
    <div class="">
        <div class="container">
            <div class="s14 about-content">
                <h1 class="s30 bold about-tit"><?php echo $page_name; ?></h1>
                <div class="<?php if( is_cart() || is_checkout() ) {} else { echo 'wp-editor-fix'; } ?>">
                    <?php the_content(); ?>
                </div>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>